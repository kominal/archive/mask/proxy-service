import { info, startObserver } from '@kominal/observer-node-client';
import { warn } from 'console';
import { createServer } from 'net';
import io from 'socket.io-client';
import { Connection, connections } from './connection';
import { BASE_URL, LISTEN_HOST, LISTEN_PORT, MASK_AUTH, NODE_HOSTNAME } from './helper/environment';
import { compareSessions } from './helper/helper';
import { Session } from './models/session';

export let lastConfigurationsString: string;
let lastSessions: Session[] = [];

export const socket = io(`wss://${BASE_URL}`, {
	transports: ['websocket'],
	path: `/controller/socket.io`,
	extraHeaders: {
		Authorization: MASK_AUTH,
	},
} as any);

export async function updateSessions(force?: boolean) {
	const sessions = connections.filter((c) => c.session).map((c) => c.session!);

	const uniqueSessions: Session[] = [];

	sessions.forEach((session) => {
		if (!uniqueSessions.find((session2) => compareSessions(session, session2))) {
			uniqueSessions.push(session);
		}
	});

	let requireSessionUpdate = force || lastSessions.length !== uniqueSessions.length;
	if (!requireSessionUpdate) {
		for (let index = 0; index < lastSessions.length; index++) {
			if (!compareSessions(lastSessions[index], uniqueSessions[index])) {
				requireSessionUpdate = true;
				break;
			}
		}
	}

	if (requireSessionUpdate) {
		lastSessions = uniqueSessions;
		socket.emit('SESSIONS', {
			sessions: uniqueSessions,
		});
	}
}

async function start() {
	startObserver();
	socket.on('connect', () => {
		info(`Established connection to controller.`);
		updateSessions(true);
		socket.emit('REGISTER', {
			nodeHostname: NODE_HOSTNAME,
			ip: LISTEN_HOST,
		});
	});
	socket.on('disconnect', () => warn(`Lost connection to controller.`));
	socket.on('connect_error', (e: any) => warn(`Error on controller connection: ${e.message}`));
	socket.on('connect_failed', (e: any) => warn(`Controller connection failed: ${e}`));

	createServer((socket) => {
		new Connection(socket);
	}).listen(LISTEN_PORT, LISTEN_HOST, () => {
		info(`Mask proxy is listening for request on ${LISTEN_HOST}:${LISTEN_PORT}.`);
	});
}

start();
