import { getEnvironmentNumber, getEnvironmentString } from '@kominal/lib-node-environment';

export const LISTEN_HOST = getEnvironmentString('LISTEN_HOST');
export const LISTEN_PORT = getEnvironmentNumber('LISTEN_PORT');
export const NODE_HOSTNAME = getEnvironmentString('NODE_HOSTNAME');
export const MASK_AUTH = getEnvironmentString('MASK_AUTH');
export const BASE_URL = getEnvironmentString('BASE_URL');
