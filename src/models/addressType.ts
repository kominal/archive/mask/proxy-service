export enum AddressType {
	IPV4 = 1,
	DOMAIN = 3,
	IPV6 = 4,
}
